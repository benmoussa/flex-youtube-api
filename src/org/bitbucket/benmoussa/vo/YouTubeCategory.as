/**
 The MIT License (MIT)

 Copyright (c) <2013> <Adil Ben Moussa [adil.benmoussa@gmail.com - https://bitbucket.org/benmoussa]>
 Based on as3 youtube api - https://code.google.com/p/as3-youtube-data-api/

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 **/
package org.bitbucket.benmoussa.vo {

	public class YouTubeCategory {
		protected var _label:String;
		protected var _term:String;
		protected var _lang:String;
		protected var _browsable:BrowsableCategory;

		public function YouTubeCategory (term:String, label:String, lang:String, browsable:BrowsableCategory) {
			_term = term;
			_label = label;
			_lang = lang;
			_browsable = browsable;
		}

		public function get term ():String {
			return _term;
		}

		public function get label ():String {
			return _label;
		}

		public function get lang ():String {
			return _lang;
		}

		public function get browsable ():BrowsableCategory {
			return _browsable;
		}

	}
}
