/**
 The MIT License (MIT)

 Copyright (c) <2013> <Adil Ben Moussa [adil.benmoussa@gmail.com - https://bitbucket.org/benmoussa]>
 Based on as3 youtube api - https://code.google.com/p/as3-youtube-data-api/

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 **/
package org.bitbucket.benmoussa.webservice {
	import ca.newcommerce.youtube.events.PlaylistFeedEvent;
	import ca.newcommerce.youtube.events.SubscriptionFeedEvent;
	import ca.newcommerce.youtube.events.VideoFeedEvent;
	import ca.newcommerce.youtube.feeds.PlaylistFeed;
	import ca.newcommerce.youtube.feeds.SubscriptionFeed;
	import ca.newcommerce.youtube.feeds.VideoFeed;

	import flash.events.EventDispatcher;

	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;

	import org.bitbucket.benmoussa.constants.YouTubeConstants;
	import org.bitbucket.benmoussa.events.CategoriesFeedEvent;
	import org.bitbucket.benmoussa.vo.BrowsableCategory;
	import org.bitbucket.benmoussa.vo.YouTubeCategory;

	/**
	 * Demonstrates basic Youtube Data API operations using the Flex client library:
	 * <ol>
	 * <li> Retrieve all available categories
	 * <li> Retrieving standard Youtube feeds
	 * <li> Searching a feed
	 * <li> Searching a feed using categories and keywords
	 * <li> Retrieving a user's uploaded videos
	 * <li> Retrieve a user's favorite videos
	 * <li> Retrieve responses for a video
	 * <li> Retrieve comments for a video
	 * <li> Retrieve a list of a user's playlists
	 * <li> Retrieve a playlist
	 * <li> Retrieve a list of a user's subscriptions
	 * <li> Retrieve a user's profile
	 * </ol>
	 *
	 * @author Adil Ben Moussa
	 */
	public class YouTubeReadonlyClient extends EventDispatcher {

		/**
		 * Class Singleton
		 */
		private static var _instance:YouTubeReadonlyClient;

		/**
		 * Get the singleton instance
		 */
		public static function getInstance ():YouTubeReadonlyClient {
			if (_instance == null)
				_instance = new YouTubeReadonlyClient();
			return _instance;
		}

		/**
		 * Inits the http service.
		 *
		 * @param url	The url to pass to the service
		 * @return 		The service token
		 */
		protected function initService (url:String):AsyncToken {
			//Init the service
			var service:HTTPService = new HTTPService();
			service.url = url;

			//fire and return the token
			return service.send();
		}

		/**
		 * Retrieves the list of all categories on youtube.
		 *
		 * Dispatches the CategoriesFeedEvent with the retrieved feed.
		 */
		public function getAllCaterories ():void {
			var token:AsyncToken = this.initService(YouTubeConstants.YOUTUBE_GDATA_CATEGORIES_SERVER);
			//result handler
			var resultHandler:Function = function (event:ResultEvent):void {
				// parse the data here
				var feed:ArrayCollection = new ArrayCollection();
				var browsableCategory:BrowsableCategory;
				var ytCategory:YouTubeCategory;
				for each (var category:Object in event.result.categories.category) {
					if (category.hasOwnProperty("browsable")) {
						browsableCategory = new BrowsableCategory(String(category.browsable.regions).split(" "));
						ytCategory = new YouTubeCategory(category.term, category.label, category["xml:lang"], browsableCategory);
						feed.addItem(ytCategory);
					}
				}
				dispatchEvent(new CategoriesFeedEvent(feed));
			};

			var resp:mx.rpc.Responder = new mx.rpc.Responder(resultHandler, faultHandler);
			token.addResponder(resp);
		}

		/**
		 * Retrieves the user's playlists feed.
		 *
		 * @param user 			The name of the owner of the playlist.
		 * @param startIndex	Result start index.
		 * @param maxResults 	Max result to return.
		 *
		 * Dispatches the PlaylistFeedEvent with the parsed feed.
		 */
		public function getPlaylists (user:String, startIndex:Number = 1, maxResults:Number = 50):void {
			var url:String = YouTubeConstants.USER_FEED_PREFIX + user + YouTubeConstants.PLAYLISTS_FEED_SUFFIX + "?alt=" + YouTubeConstants.GDATA_RESULT_FORMAT + "&start-index=" + startIndex + "&max-results=" +
				maxResults
			var token:AsyncToken = this.initService(url);
			//result handler
			var resultHandler:Function = function (event:ResultEvent):void {
				// parse the data here
				//trace("result" + event.result);
				var feed:PlaylistFeed = new PlaylistFeed(event.result as String);
				dispatchEvent(new PlaylistFeedEvent(PlaylistFeedEvent.PLAYLIST_DATA_RECEIVED, 0, user, feed));

			};
			var resp:mx.rpc.Responder = new mx.rpc.Responder(resultHandler, faultHandler);
			token.addResponder(resp);
		}

		/**
		 * Retrieves the user's subscriptions feed.
		 *
		 * @param user 			The name of the owner of the subscription.
		 * @oaram categories	Possible categories
		 * @param startIndex	Result start index.
		 * @param maxResults 	Max result to return.
		 *
		 * Dispatches the SubscriptionFeedEvent with the parsed feed.
		 */
		public function getSubscriptions (user:String, categories:Array = null, startIndex:Number = 1, maxResults:Number = 50):void {
			var url:String = YouTubeConstants.USER_FEED_PREFIX + user + YouTubeConstants.SUBSCRIPTIONS_FEED_SUFFIX;
			if (categories && categories.length > 0)
				url += "/-/" + categories.join("%7C");
			url += "?alt=" + YouTubeConstants.GDATA_RESULT_FORMAT + "&start-index=" + startIndex + "&max-results=" + maxResults;
			var token:AsyncToken = this.initService(url);
			//result handler
			var resultHandler:Function = function (event:ResultEvent):void {
				// parse the data here
				//trace("result" + event.result);
				var feed:SubscriptionFeed = new SubscriptionFeed(event.result as String);
				dispatchEvent(new SubscriptionFeedEvent(SubscriptionFeedEvent.SUBSCRIPTION_DATA_RECEIVED, 0, feed));

			};
			var resp:mx.rpc.Responder = new mx.rpc.Responder(resultHandler, faultHandler);
			token.addResponder(resp);
		}


		/**
		 * Retrieves the user's favorites feed.
		 *
		 * @param user 			The name of the owner of the subscription.
		 * @oaram categories	Possible categories
		 * @oaram keywords		Possible keywords
		 * @param startIndex	Result start index.
		 * @param maxResults 	Max result to return.
		 *
		 * Dispatches the VideoFeedEvent with the parsed feed.
		 */
		public function getFavorites (user:String, categories:Array = null, keywords:Array = null, startIndex:Number = 1, maxResults:Number = 50):void {

			var url:String = YouTubeConstants.USER_FEED_PREFIX + user + YouTubeConstants.FAVORITES_FEED_SUFFIX;
			if (categories && categories.length > 0)
				url += "/-/" + categories.join("%7C");

			if (keywords && keywords.length > 0) {
				if (url.indexOf("/-/") == -1)
					url += "/-";
				url += "/" + keywords.join("%7C");
			}
			url += "?alt=" + YouTubeConstants.GDATA_RESULT_FORMAT + "&start-index=" + startIndex + "&max-results=" + maxResults;
			var token:AsyncToken = this.initService(url);
			//result handler
			var resultHandler:Function = function (event:ResultEvent):void {
				// parse the data here
				//trace("result" + event.result);
				var feed:VideoFeed = new VideoFeed(event.result as String);
				dispatchEvent(new VideoFeedEvent(VideoFeedEvent.USER_FAVORITES_DATA_RECEIVED, 0, feed));

			};
			var resp:mx.rpc.Responder = new mx.rpc.Responder(resultHandler, faultHandler);
			token.addResponder(resp);
		}

		/**
		 * Retrieves the user's uploads feed.
		 *
		 * @param user 			The name of the owner of the subscription.
		 * @oaram categories	Possible categories
		 * @oaram keywords		Possible keywords
		 * @param startIndex	Result start index.
		 * @param maxResults 	Max result to return.
		 *
		 */
		/*public function getUploads (user:String, categories:Array = null, keywords:Array = null, startIndex:Number = 1, maxResults:Number = 50):void {
		}*/




		/**
		 * General fault handler
		 */
		public function faultHandler (event:FaultEvent):void {
			//trace(event);
			Alert.show(event.fault.faultString, "Fault(" + event.fault.faultCode + ")");
		}
	}
}
