/**
 The MIT License (MIT)

 Copyright (c) <2013> <Adil Ben Moussa [adil.benmoussa@gmail.com - https://bitbucket.org/benmoussa]>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 **/
package org.bitbucket.benmoussa.constants {

	public class YouTubeConstants {

		/**
		 * The result data format expected from the gdata server
		 */
		public static const GDATA_RESULT_FORMAT:String = "json"; //possible values (json, xml)

		/**
		 * The name of the server hosting the YouTube GDATA feeds
		 */
		public static const YOUTUBE_GDATA_SERVER:String = "http://gdata.youtube.com";

		/**
		 * The name of the url of the youtube categories
		 */
		public static const YOUTUBE_GDATA_CATEGORIES_SERVER:String = "http://gdata.youtube.com/schemas/2007/categories.cat";

		/**
		 * The prefix common to all standard feeds
		 */
		public static const STANDARD_FEED_PREFIX:String = YOUTUBE_GDATA_SERVER
			+ "/feeds/api/standardfeeds/";

		/**
		 * The URL of the "Most Recent" feed
		 */
		public static const MOST_RECENT_FEED:String = STANDARD_FEED_PREFIX
			+ "most_recent";

		/**
		 * The URL of the "Top Rated" feed
		 */
		public static const TOP_RATED_FEED:String = STANDARD_FEED_PREFIX
			+ "top_rated";

		/**
		 * The URL of the "Most Viewed" feed
		 */
		public static const MOST_VIEWED_FEED:String = STANDARD_FEED_PREFIX
			+ "most_viewed";

		/**
		 * The URL of the "Recently Featured" feed
		 */
		public static const RECENTLY_FEATURED_FEED:String = STANDARD_FEED_PREFIX
			+ "recently_featured";

		/**
		 * The URL of the "Watch On Mobile" feed
		 */
		public static const WATCH_ON_MOBILE_FEED:String = STANDARD_FEED_PREFIX
			+ "watch_on_mobile";

		/**
		 * The URL of the "Videos" feed
		 */
		public static const VIDEOS_FEED:String = YOUTUBE_GDATA_SERVER
			+ "/feeds/api/videos";

		/**
		 * The prefix of the User Feeds
		 */
		public static const USER_FEED_PREFIX:String = YOUTUBE_GDATA_SERVER
			+ "/feeds/api/users/";

		/**
		 * The URL suffix of the test user's uploads feed
		 */
		public static const UPLOADS_FEED_SUFFIX:String = "/uploads";

		/**
		 * The URL suffix of the test user's favorites feed
		 */
		public static const FAVORITES_FEED_SUFFIX:String = "/favorites";

		/**
		 * The URL suffix of the test user's subscriptions feed
		 */
		public static const SUBSCRIPTIONS_FEED_SUFFIX:String = "/subscriptions";

		/**
		 * The URL suffix of the test user's playlists feed
		 */
		public static const PLAYLISTS_FEED_SUFFIX:String = "/playlists";
	}
}
